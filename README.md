# Reproducibility of Build Environments through Space and Time

## General instructions

This repository is the code archive for the paper `Reproducibility of Build Environments through Space and Time` available [here](https://doi.org/10.1145/3639476.3639767). 
This repository is split in several experiences associated with the steps described in the paper. They can be found in the `experiences` folder.
To run the scripts provided in the `experiences` folder, one can use the reproducible build environment provided by nix by running `nix develop`.
Machine specific settings have to be provided in the `settings.py` file.

We provide a `data` (`data.zip`) archive containing various caches (evaluation cache, hydra pages cache, etc). A reader wanting to reproduce the results described in the paper may run the experiments with (or partially with) or without this `data` folder to reproduce the experiments with various depth degrees. 
The complete archive including the `data.zip` file is available on Zenodo at [this address](https://doi.org/10.5281/zenodo.10519820).


## Experiences 

### 1. Job list reproducibility

The experiment will perform local evaluation of the sampled revisions and check for job list reproducibility.
Run the script `compare_job_lists.py` to run it. This is a computationally heavy task, as it needs to evaluate 200 past revisions of `nixpkgs`. Our own evaluation results are located in `data/eval_cache`. 

### 2. Build environments reproducibility

This experiment will check for output paths discrepencies betweeen our local evaluation and the one performed by Hydra. To run it, run the `find_diff_build_env.py` script. This implies having run experiment 1, or it will perform the evaluations. This will also download a lot of build pages from Hydra. One may want to instead use the provided `data` folder that contains a `build_ids_cache` folder. 

### 3. Nix builds rebuildability

This experience rebuilds the Nixpkgs revision `53281023253de9962d9b99b900690f194719c7c2`. The script `schedule_builds.py` has to be run first. This is a heavy in ressources operation as it will build around 14000 packages. The script `compute_res.py` will then analyse the results of the build. Our own build results are located in `data/build_cache`.

