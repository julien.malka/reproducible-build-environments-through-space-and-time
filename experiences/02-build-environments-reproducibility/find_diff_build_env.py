from os import sys, path
import shutil
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
from settings import *
from lib import fetch_hydra
from lib import eval
from lib import drv
from bs4 import BeautifulSoup
import sys
import json
import os
from multiprocessing import Pool
from functools import partial
from pathlib import Path
from dataclasses import dataclass
import subprocess
from typing import List

def get_eval_nb(eval):
    return eval.split(" ")[0].strip()

def get_rev(eval):
    return eval.split(" ")[1].strip()

@dataclass(frozen=True)
class Derivation:
    store_path: str
    out_path: str

@dataclass
class EvalReproducibilityProblem:
    nix_expression_affected: str
    jobs_from_affected_expression: List[str]
    first_eval_affected: int 
    last_eval_affected: int

# List of all the reproducibility problems that we identified
reproducibility_problems = [
        # irods
        # Expression uses builtins.nexVersion, which evaluates differently following the version of Nix used
        # Introduced in https://github.com/NixOS/nixpkgs/pull/19898
        # Fixed in https://github.com/NixOS/nixpkgs/pull/156527
        EvalReproducibilityProblem("pkgs/tools/filesystems/irods/common.nix", ["irods"], 1377834, 1738173),
        # The nixpkgs expression defined in pkgs/top-level/release.nix is setup to embbed the rev number on
        # hydra but is defaulting to "abcdef". 
        EvalReproducibilityProblem("pkgs/top-level/release.nix", ["nixpkgs-tarball", "nixpkgs-metrics", "nixpkgs-manual"], 1377834, 1794205),
        # basex
        # The basex expression had a reference to a local path, evaluating differently on different machines
        # Introduced in https://github.com/NixOS/nixpkgs/pull/68339
        # Fixed in https://github.com/NixOS/nixpkgs/pull/137699
        EvalReproducibilityProblem("pkgs/tools/text/xml/basex/default.nix", ["basex"], 1621676, 1704125),
        # docbookrx
        # The docbookrx expression had a reference to a local path, evaluating differently on different machines
        # Introduced in https://github.com/NixOS/nixpkgs/pull/147643
        # Fixed in https://github.com/NixOS/nixpkgs/pull/154742
        EvalReproducibilityProblem("pkgs/tools/typesetting/docbookrx/gemset.nix", ["docbookrx"], 1726352, 1737138),
        # nixpkgs-lib-tests
        # The nixpkgs-lib-tests expression uses pkgs.path, that evauated differently following the path the nixpkgs folder is stored in. 
        # Introduced in https://github.com/NixOS/nixpkgs/pull/6242
        # Fixed in https://github.com/NixOS/nixpkgs/pull/82461
        EvalReproducibilityProblem("lib/tests/release.nix", ["nixpkgs-lib-tests"], 1377834, 1582113),
        # Linux kernel
        # The expression for the linux kernel depends on lib.inNixShell and as such evaluates differently 
        # in and out of a Nix shell.
        # Introduced in https://github.com/NixOS/nixpkgs/pull/47909
        # Fixed in https://github.com/NixOS/nixpkgs/pull/51833
        EvalReproducibilityProblem("pkgs/os-specific/linux/kernel/manual-config.nix", [
            "linux-4", 
            # NixOS tests and systemtap depend transitively on the kernel
            "systemtap", 
            "test" ], 1485900, 1493785),
        # Etc declarative config 
        # The script for the generation of the etc folder in NixOS used 
        # escapeShellArg, which used toString on the files to copy and will hence
        # copy their absolute path instead of copying to the store.
        # Only affect the nixos tests since this is NixOS configuration realm.
        # Introduced in https://github.com/NixOS/nixpkgs/pull/131102
        # Fixed in https://github.com/NixOS/nixpkgs/pull/136474
        EvalReproducibilityProblem("nixos/modules/system/etc/etc.nix", ["test"], 1691280, 1701084),
    ]


def parse_drv(path):
    """
    Find all the input_drvs of a given drv file by parsing it
    """
    # Try for nix store drv file, which means local evaluation has happened
    final_path = ""
    if os.path.isfile(path):
        final_path = path
        file = path.split("/")[-1]
        final_path = f"{datadir}/drv_cache/{file}"
        shutil.copyfile(path, final_path)
    # Otherwise rely on the cache
    else:
        file = path.split("/")[-1]
        final_path = f"{datadir}/drv_cache/{file}"
    with open(final_path, 'r') as f:
        return set(drv.drvparse(f.read()).input_drvs.keys())


def get_build_ids(eval_nb):
    """
    Get all hydra build_ids of a given evaluation
    """
    eval_page = fetch_hydra.fetch_eval(eval_nb, json=False)
    ids = {}
    soup = BeautifulSoup(eval_page, "html.parser")
    jobs = set()
    for table in soup.find_all('tbody'):
        for row in table.find_all('tr'):
            cols = row.find_all('td')
            # Skip invalid rows
            if not cols[0].find('img'):
                continue
            name = cols[2].find('a').get_text()
            ids[name] = cols[1].find('a').get_text()
    return ids


def get_drv_hydra(build_id):
    """
    Get the store_path and out_path associated with 
    a given build_id, as found by Hydra.
    """
    contents = fetch_hydra.fetch_build(build_id)
    res = json.loads(contents)
    return Derivation(res["store_path"], res["out_path"])

def get_drv_nix_eval_jobs(eval_nb):
    """
    Computes a map given the nix-eval-jobs output of an evaluation 
    job_name -> Drv 
    """
    job_to_drv = {}
    f = open(f"{datadir}/eval_cache/{eval_nb}/nix-eval-jobs-out.txt")
    lines = f.readlines()
    for line in lines:
        line_json = json.loads(line)
        keys = line_json.keys()
        if "error" not in keys and "drvPath" in keys:
            job_name = line_json["attr"]
            drv_path = line_json["drvPath"]
            job_to_drv[job_name] = Derivation(drv_path, list(line_json["outputs"].values())[0]) # Here handle multiple outputs ?
    return job_to_drv


def check_for_problem(drv, eval_nb):
    """
    Checks if a drv has been identified as problem for specified eval
    """
    for problem in reproducibility_problems:
            for job in problem.jobs_from_affected_expression:
                if job in drv and int(eval_nb) >= problem.first_eval_affected and int(eval_nb) <= problem.last_eval_affected: 
                    return True
    return False

def compare_drvs_eval(eval_nb):
    """
    Given an eval, this function identifies all jobs 
    for which the out_path is different than the one obtained by Hydra
    It then remove from the list: 
     - jobs that have been identified as problems 
     - and their dependents
    """
    # Create the result folder
    p = Path(f"results/{eval_nb}")
    p.mkdir(parents=True, exist_ok=True)

    job_to_build_id = get_build_ids(eval_nb)
    job_to_drv = get_drv_nix_eval_jobs(eval_nb)
    res_out_file = f"results/{eval_nb}/report_out.txt" 
    report_file = f"results/{eval_nb}/report.json" 

    differing_local = []
    differing_hydra = []
    # First, compute all diverging store paths
    for job in job_to_drv.keys():
        res = compare_drv(job_to_drv, job_to_build_id, job)
        if res is not None:
            local, hydra = res
            differing_local.append(local)
            differing_hydra.append(hydra)
    differing_local = list(dict.fromkeys(differing_local))
    differing_hydra = list(dict.fromkeys(differing_hydra))
    differing_local_store_path = [a.store_path for a in differing_local]

    # Count the number of job in eval matching eval reproducibility problems
    matching_problems = []
    for drv in differing_local:
        if check_for_problem(drv.store_path, eval_nb):
            matching_problems.append(drv)

    # Find out all jobs that should be removed from the list 
    # Converging algorithm because we can iteratively add 
    # dependent jobs
    do_loop = True
    to_remove = []
    while do_loop:
        new_elems = []
        to_remove_drvs = [ a.store_path for a in to_remove]
        for drv in differing_local:
            # First check if drv an "evaluation problem" 
            if check_for_problem(drv.store_path, eval_nb):
                if drv not in to_remove:
                    new_elems.append(drv)
            dependencies = parse_drv(drv.store_path)
            for dependency in dependencies:
                if check_for_problem(dependency, eval_nb) or dependency in to_remove_drvs:
                    if drv not in to_remove:
                        new_elems.append(drv)
        if new_elems == []:
            do_loop = False
        else:
            to_remove = to_remove + new_elems

    # remove elements from differing lists
    for elem in set(to_remove):
        try:
            i = differing_local.index(elem)
            del differing_local[i]
            del differing_hydra[i]
        except:
            pass

    to_write = [ f"{a.out_path} {b.out_path}" for (a,b) in zip(differing_local, differing_hydra)]
    if len(to_write) > 0:
        with open(res_out_file, 'w') as g:
            g.write("\n".join(to_write) + "\n")
    with open(report_file, 'w') as h:
        h.write(json.dumps({"total_jobs": len(job_to_drv.keys()), "reproductibility_errors":len(matching_problems), "uncaught_errors": len(to_write)})) 
     


def compare_drv(job_to_drv, builds_ids, job_name):
    """
    Given a job name, compare output path from 
    Hydra and local eval
    """
    if job_name not in builds_ids.keys():
        return None
    res = ""
    build_id = builds_ids[job_name]
    drv_hydra = get_drv_hydra(build_id)
    drv_local = job_to_drv[job_name]
    if drv_local.out_path != drv_hydra.out_path:
        return drv_local, drv_hydra
    else:
        return None

def check_eval(eval):
    eval_nb = get_eval_nb(eval)
    rev = get_rev(eval)
    res_path = f"results/{eval_nb}/report.json" 
    # If we already have the result for this evaluation we can skip it
    if not path.exists(res_path):
        if os.path.isfile(f"{datadir}/eval_cache/{eval_nb}/nix-eval-jobs-out.txt"):
            print(f"checking for outPath reproducibility for eval {eval_nb}")
            compare_drvs_eval(eval_nb)
        else:
            print(f"eval {eval_nb} local evaluation is missing")


if __name__ == '__main__':
    f = open("../../sampled_eval", "r")
    evals = f.readlines()
    # First make sure all local evals exists
    # We can't do that in check_eval because parallelism 
    for eval_ in evals:
        cache_dir = f"{datadir}/eval_cache/{get_eval_nb(eval_)}"
        if not os.path.exists(cache_dir):
            eval.cache_or_do_eval(cache_dir, get_rev(eval_), "pkgs/top-level/release.nix", "nix-eval-jobs-out.txt")

    with Pool(workers) as p:
        p.map(check_eval, list(evals))
    uncaught_errors = 0
    errors = 0
    total_jobs = 0
    for eval in evals:
        f = open(f"results/{get_eval_nb(eval)}/report.json")
        data = json.load(f)
        uncaught_errors += data["uncaught_errors"]
        errors += data["reproductibility_errors"]
        total_jobs += data["total_jobs"]
    print(f"Total uncaught_errors: {uncaught_errors}")
    print(f"Proportion of non reproducible job evaluation: {errors/total_jobs*100}%")
    print(f"Proportion of reproducible job evaluation: {100 - errors/total_jobs*100}%")
    

    
