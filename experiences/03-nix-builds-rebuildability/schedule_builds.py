import os
os.sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
from settings import *
from lib import eval, drv
import multiprocessing
import time
import json
import subprocess
from pathlib import Path
import shutil 

unreproducible_evaluation_jobs = [ "irods", "nixpkgs-tarball", "nixpkgs-metrics", "nixpkgs-manual", "nixpkgs-lib-tests", "nixpkgs-17" ]


def data_cache_path(outputs):
    """
    Compute a cache path based of the output hash of out output of derivation
    """
    out = outputs["out"]["path"]
    res = "".join(out.split("/")[-1]) 
    return res

def check_if_built(path):
    """
    Check if a path has already been built
    """
    outputs = parse_drv(path)
    cache_path = data_cache_path(outputs)
    for output in outputs:
        cache_path_output = cache_path + f":{output}"
        if not os.path.isfile(f"{datadir}/build_cache/{cache_path_output}/metadata.txt"):
            print(f"Path {path} is not yet built")
            return False
    print(f"Path {path} is already built") 
    return True

def write_metadata(metadata, cache_path):
    """
    Write build metadata to cache 
    """
    json_dump = json.dumps(metadata, indent = 4) 
    met = open(f"{datadir}/build_cache/{cache_path}/metadata.txt", "w")
    met.write(json_dump)
    met.close()

def build_path(path):
    """
    Build the given drv
    """
    # Is this path from unreproducible_evaluation, and us such not in cache
    not_reproducible_evaluation = False
    for job in unreproducible_evaluation_jobs:
        if job in path:
            not_reproducible_evaluation = True
    outputs = parse_drv(path)
    cache_path = data_cache_path(outputs)
    built_already = check_if_built(path) 
    if not built_already:
        print(f"Building {path}")
        # We have to use --check here to do only the build step 
        args = ["nix-build", path]
        if not not_reproducible_evaluation:
            args += ["--check", "--keep-failed"]
        process = subprocess.Popen(args, stdout=subprocess.DEVNULL, stderr=subprocess.PIPE, encoding='utf-8')
        start = 0
        logs = ""
        metadata = {}
        for line in iter(process.stderr.readline, b''):
            if not line: break
            logs += line
            if "unpacking sources" in line:
                start = time.time()
        process.stderr.close()
        rc = process.wait() 
        metadata = {}
        end = time.time()

        for output in outputs:
            cache_path_output = cache_path + f":{output}"
            folder = Path(f"{datadir}/build_cache/{cache_path_output}")
            folder.mkdir(parents=True, exist_ok=True)
            metadata = {}
            metadata["time"] = end - start
            if rc != 0:
                non_repro = False
                for o in outputs:
                    if os.path.exists(outputs[o]["path"]+".check"):
                        non_repro = True
                if non_repro:
                    metadata["status"] = "buildable"
                else:
                    metadata["status"] = "failed"
            else:
                metadata["status"] = "buildable"

            write_metadata(metadata, cache_path_output)

            if metadata["status"] == "failed":
                with open(f"{datadir}/build_cache/{cache_path_output}/local_log.txt", "w") as f_logs:
                    f_logs.write(logs)
                with open(f"{datadir}/build_cache/{cache_path_output}/cache_log.txt", "w") as f_logs_cache:
                    subprocess.call(["nix", "log", "--store", "https://cache.nixos.org", outputs[output]["path"]], stdout=f_logs_cache, stderr=subprocess.DEVNULL)
        print(f"Built {path}")


def parse_drv(path):
     der = subprocess.check_output(f"nix derivation show '{path}^*'", shell=True).decode("utf-8")
     der = json.loads(der)[path]
     out = der["outputs"]
     return out


def read_eval(eval_nb):
    jobs = []
    f = open(f"{datadir}/eval_cache/{eval_nb}/nix-eval-jobs-out.txt")
    lines = f.readlines()
    for line in lines:
        line_json = json.loads(line)
        keys = line_json.keys()
        if "error" not in keys:
            if line_json["system"] == "x86_64-linux":
                jobs.append(line_json["drvPath"])

    return list(jobs)


def get_remaining_to_build(build_plan):
    """
    Function used when we restart the build of an eval 
    """
    to_build = []
    for build in build_plan:
        built_already = check_if_built(build)
        if not built_already:
            to_build.append(build)
    return to_build


def main():
    print("Number of cpu : ", multiprocessing.cpu_count())

    eval_nb = 1377834
    eval_rev = "53281023253de9962d9b99b900690f194719c7c2"
    number_of_processes = 50
    cache_dir = f"{datadir}/eval_cache/{eval_nb}"
    # Do the evaluation if it is not yet present
    eval.cache_or_do_eval(cache_dir, eval_rev, "pkgs/top-level/release.nix", "nix-eval-jobs-out.txt")

    # Get the list of jobs to build for that evaluation
    builds = read_eval(eval_nb)
    # Get the list of jobs that are remaining to build
    to_build = get_remaining_to_build(builds)

    number_of_task = len(to_build)
    print(f"Number already built : {len(builds) - len(to_build)}")
    print(f"Number of tasks to build: {number_of_task}") 
    
    # Build all the jobs parallely 
    with multiprocessing.Pool(number_of_processes) as p:
        p.map(build_path, to_build)


if __name__ == '__main__':
    main()
