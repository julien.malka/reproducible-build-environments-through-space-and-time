from os import sys, path
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
from settings import *
from lib import eval
from lib import fetch_hydra
from lib import drv
import multiprocessing
from multiprocessing import Lock, Process, Queue, current_process, Pool, Manager
import time
import queue
import os
import random
import json
import subprocess
from pathlib import Path
import shutil 
import random
from bs4 import BeautifulSoup


def read_eval(eval_nb):
    jobs = []
    f = open(f"{datadir}/eval_cache/{eval_nb}/nix-eval-jobs-out.txt")
    lines = f.readlines()
    for line in lines:
        line_json = json.loads(line)
        keys = line_json.keys()
        if "error" not in keys:
            if line_json["system"] == "x86_64-linux":
                jobs.append(line_json["drvPath"])

    return list(jobs)

def parse_drv(path):
    # Try for nix store drv file, which means local evaluation has happened
     final_path = ""
     if os.path.isfile(path):
        final_path = path
        file = path.split("/")[-1]
        final_path = f"{datadir}/drv_cache/{file}"
        shutil.copyfile(path, final_path)
     # Otherwise rely on the cache
     else:
        file = path.split("/")[-1]
        final_path = f"{datadir}/drv_cache/{file}"
    
     with open(final_path, 'r') as f:
        return drv.drvparse(f.read()).outputs

def data_cache_path(outputs):
    out = outputs["out"].path
    res = "".join(out.split("/")[-1]) 
    return res


def main():
    eval_nb = 1377834
    eval_rev = "53281023253de9962d9b99b900690f194719c7c2"
    cache_dir = f"{datadir}/eval_cache/{eval_nb}"
    builds = read_eval(eval_nb)
    failed = []
    for build in builds: 
        outputs = parse_drv(build)
        cache_path = data_cache_path(outputs)
        for output in outputs:
            cache_path_output = cache_path + f":{output}"
            if not os.path.isfile(f"{datadir}/build_cache/{cache_path_output}/metadata.txt"):
                print(f"{datadir}/build_cache/{cache_path_output}/metadata.txt")
                print(f"Non built output {cache_path_output}")
            else:
                meta = open(f"{datadir}/build_cache/{cache_path_output}/metadata.txt")
                meta = json.load(meta)
                if meta["status"] == "failed":
                    failed.append(build)
                    break
    print(f"Packages to build: {len(builds)}")
    print(f"Failed to build: {len(failed)}")
    eval_page = fetch_hydra.fetch_eval(eval_nb, json=False)
    soup = BeautifulSoup(eval_page, "html.parser")
    jobs_status = {}
    for table in soup.find_all('tbody'):
        for row in table.find_all('tr'):
            cols = row.find_all('td')
            # Skip invalid rows
            if not cols[0].find('img'):
                continue
            name = cols[2].find('a').get_text()
            system = cols[5].find('tt').get_text()
            if "x86_64-linux" in system:
                jobs_status[name] = cols[0].find('img')['title']

    f = open(f"{datadir}/eval_cache/{eval_nb}/nix-eval-jobs-out.txt")
    drv_to_name = {}
    lines = f.readlines()
    for line in lines:
        line_json = json.loads(line)
        keys = line_json.keys()
        if "error" not in keys:
            job_name = line_json["attr"]
            if "x86_64-linux" in line_json["system"]:
                drv = line_json["drvPath"]
                drv_to_name[drv] = job_name

    new_fails = set()
    failed_but_hydra_failed = []
    for fail in failed:
        print(f"Failed to rebuild {fail}, hydra status: {jobs_status[drv_to_name[fail]]}")
        if jobs_status[drv_to_name[fail]] == "Succeeded":
            new_fails.add(fail)
        else:
            failed_but_hydra_failed.append(fail)
    print(f"New fails: {len(new_fails)}")
    job_correc_hydra = len(builds) - len(failed_but_hydra_failed)
    print(f"Jobs that were correctly built by Hydra: {job_correc_hydra}")
    print(f"Proportion new fails = {len(new_fails) / job_correc_hydra * 100}%")
    print(f"Proportion that rebuilds = {100 - len(new_fails) / job_correc_hydra * 100}%")
    print(new_fails)


if __name__ == '__main__':
    main()
