{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/master";
    nixpkgs_with_nix_2_6.url = "github:NixOS/nixpkgs/771911a16edf7a0a35be8318ed5162ec1d44ca14";
    nix-eval-jobs-flake = {
      url = "github:nix-community/nix-eval-jobs/5bff47314af794cd2c3771b9bd559c63ab389c8c";
    };

  };

  outputs = inputs @ { self, nixpkgs, nix-eval-jobs-flake, ... }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      pkgs_2_6 = import inputs.nixpkgs_with_nix_2_6 {
        system = "x86_64-linux";
      };
    in
    {
      devShell.x86_64-linux =
        pkgs.mkShell {
          shellHook = ''
            ulimit -s 50000
          '';
          buildInputs = with pkgs; [
            ((nix-eval-jobs-flake.packages.x86_64-linux.nix-eval-jobs.overrideAttrs (_:
              {
                patches = [ ./nev.patch ];
              }
            )).override ({
              nix = pkgs_2_6.nix;
              stdenv = pkgs_2_6.stdenv;
            }))
            (python3.withPackages
              (ps: with ps; [ requests clint beautifulsoup4 GitPython multiprocess ]))
            xz
          ];

        };
    };
}


