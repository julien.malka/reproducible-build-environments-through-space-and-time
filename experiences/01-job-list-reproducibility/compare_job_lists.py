from os import sys, path
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
from settings import *
from lib import fetch_hydra
from lib import eval
from bs4 import BeautifulSoup
import json
from pathlib import Path
import re


def get_eval_nb(eval):
    """Read the eval number from an evaluation"""
    return eval.split(" ")[0].strip()

def get_rev(eval):
    """Read the revision from an evaluation"""
    return eval.split(" ")[1].strip()


def filter_jobs_with_quotes(jobs, eval_nb):
    """Filter out jobs that contains a dot in their name,
    but only if they are after eval 1471085"""
    res = set()
    for job in jobs:
        if '"' not in job:
            res.add(job)
        else:
            # Hydra started dropping dot jobs after this eval
            if int(eval_nb) < 1471085:
                res.add(job.replace('"', ""))
    return res

def job_list_from_local(eval_path, eval_nb):
    """Read the job list from local evaluation"""
    f = open(eval_path)
    lines = f.readlines()
    jobs = set()
    for line in lines:
        line_json = json.loads(line)
        keys = line_json.keys()
        if "error" not in keys:
            job_name = line_json["attr"]
            if "x86_64-linux" in job_name:
                jobs.add(job_name)
    f.close()
    return filter_jobs_with_quotes(jobs, eval_nb)

def job_list_from_hydra(eval_nb):
    """Read the job list from hydra evaluation"""
    eval_page = fetch_hydra.fetch_eval(eval_nb, json=False)
    soup = BeautifulSoup(eval_page, "html.parser")
    jobs = set()
    for table in soup.find_all('tbody'):
        for row in table.find_all('tr'):
            cols = row.find_all('td')
            # Skip invalid rows
            if not cols[0].find('img'):
                continue
            name = cols[2].find('a').get_text()
            if "x86_64-linux" in name:
                jobs.add(cols[2].find('a').get_text())
    return jobs

def compare_job_lists(eval_nb, cache_dir):
    hydra = job_list_from_hydra(eval_nb)
    local = job_list_from_local(f"{cache_dir}/nix-eval-jobs-out.txt", eval_nb)
    report = {}
    report["jobs"] = {}
    report["jobs"]["hydra"] = len(hydra)
    report["jobs"]["local"] = len(local)
    not_in_hydra = 0
    not_in_hydra_list = []
    for err  in local:
        if err not in hydra:
            not_in_hydra += 1
            not_in_hydra_list.append(err)
    report["jobs"]["local_not_in_hydra"] = not_in_hydra
    not_in_local = 0
    not_in_local_list  = []
    for err in hydra:
        if err not in local:
            not_in_local += 1
            not_in_local_list.append(err)
    report["jobs"]["hydra_not_in_local"] = not_in_local
    p = Path(f"results/{eval_nb}")
    p.mkdir(parents=True, exist_ok=True)
    if len(not_in_hydra_list) > 0:
        f1 = open(f"results/{eval_nb}/not_in_hydra", "w")
        f1.write("\n".join(not_in_hydra_list))
        f1.close()
    if len(not_in_local_list) > 0:
        f2 = open(f"results/{eval_nb}/not_in_local", "w")
        f2.write("\n".join(not_in_local_list))
        f2.close()
    return report 


def main():
    f = open("../../sampled_eval", "r")
    lines = f.readlines()
    total_hydra_not_in_local = 0
    total_local_not_in_hydra = 0
    for line in lines:
        eval_rev = get_rev(line)
        eval_nb = get_eval_nb(line)
        cache_dir = f"{datadir}/eval_cache/{eval_nb}"
        result_file = f"results/{eval_nb}/report.json"
        if not path.exists(result_file):
            eval.cache_or_do_eval(cache_dir, eval_rev, "pkgs/top-level/release.nix", "nix-eval-jobs-out.txt")
            report = compare_job_lists(eval_nb, cache_dir)
            report_txt = json.dumps(report, indent=4)
            p = Path(f"results/{eval_nb}")
            p.mkdir(parents=True, exist_ok=True)
            f = open(result_file, "w")
            f.write(report_txt)
            f.close()
        with open(result_file) as f:
            data = json.load(f)
            total_hydra_not_in_local += data["jobs"]["hydra_not_in_local"]
            total_local_not_in_hydra =  data["jobs"]["local_not_in_hydra"]
    print(f"Total local jobs not in hydra: {total_local_not_in_hydra}")
    print(f"Total hydra jobs not in local: {total_hydra_not_in_local}")



        

if __name__ == '__main__':
    main()
    
