import os, shutil
import requests
import json
from pathlib import Path
from clint.textui import progress
from settings import *
from bs4 import BeautifulSoup

base_url = "https://hydra.nixos.org/"


def generate_path(url, json=True):
    """Generate the local path where a given page is present if cached"""
    os_path = f"{datadir}/hydra_cache/" + url + "/out" 
    if json:
        os_path += ".json"
    else:
        os_path += ".html"
    return os_path


def download(url, json=True):
    """Download an hydra page"""
    os_path = generate_path(url, json)
    if not json:
        url += "?full=1"
    headers = {}
    if json:
        headers = {'Content-Type': 'application/json'} 
    r = requests.get(base_url + url , headers=headers, stream=True)
    with open(os_path, 'wb') as f:
        total_length = int(r.headers.get('content-length'))
        for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1): 
            if chunk:
                f.write(chunk)
                f.flush()

    

def download_or_cache(url, json=True):
    """Download an hydra page if not present in cache"""
    os_path = generate_path(url, json)
    if not os.path.isfile(os_path):
        path = Path(f"{datadir}/hydra_cache/" + url)
        path.mkdir(parents=True, exist_ok=True)
        download(url, json)
    f = open(os_path, "r")
    contents = f.read()
    return contents
        

def fetch_eval(eval_nb, json=True):
    return download_or_cache(f"eval/{eval_nb}", json)

def fetch_build(build_id):
    """
    Fetch store_path and out_path corresponding to a build_id on Hydra.
    We store a cache of (stripped down for storage) hydra pages. 
    """
    os_path = f"{datadir}/build_ids_cache/{build_id}"
    if not os.path.isfile(os_path):
        path = Path(f"{datadir}/build_ids_cache")
        path.mkdir(parents=True, exist_ok=True)
        # Download Hydra's page
        page = download_or_cache(f"build/{build_id}", json=False)
        # Extract store_path, out_path
        soup = BeautifulSoup(page, "html.parser")
        details = soup.find("div", {"id": "tabs-details"})
        rows = details.find_all('tr')
        drvPath = ""
        outPath = ""
        for row in rows:
            if row.find('th').text == "Derivation store path:":
                drvPath = row.find('td').text
            if row.find('th').text == "Output store paths:":
                outPath = row.find('td').text.split(",")[0]

        res = {"build_id": build_id, "store_path":drvPath, "out_path": outPath} 
        json_res = json.dumps(res)
        with open(os_path, 'w') as f:
            f.write(json_res)
        shutil.rmtree(f"{datadir}/hydra_cache/build/{build_id}")
    f = open(os_path, 'r')
    contents = f.read()
    return contents

