import os
from os import sys, path
import subprocess
from lib import nixpkgs
from pathlib import Path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import settings

def do_nix_eval_jobs(cache_dir, eval_rev, workers, expr, out_file):
    """
    Invoques nix-eval-jobs to perform local evaluation of one revision
    Hyper-parameters like workers and max-memory-size may be adapted
    in function of the hardware you are using.
    """
    path = Path(cache_dir)
    path.mkdir(parents=True, exist_ok=True)
    nixpkgs.clone_nixpkgs(eval_rev, cache_dir)
    out = open(f"{cache_dir}/{out_file}.tmp", "w")
    subprocess.run(['nix-eval-jobs', '--workers', str(workers), '--force-recurse', '--max-memory-size', '4000', f'{cache_dir}/nixpkgs/{expr}'], check=True, text=True, stdout=out, stderr=subprocess.DEVNULL)
    subprocess.run(['mv', f'{cache_dir}/{out_file}.tmp', f'{cache_dir}/{out_file}'], check=True, text=True) 


def cache_or_do_eval(cache_dir, eval_rev, expr, out_file):
    """
    Check if we already have a local evaluation or perform
    it otherwise.
    """
    if not os.path.isfile(f"{cache_dir}/{out_file}"):
        print(f"Evaluating nixpkgs revision {eval_rev}")
        do_nix_eval_jobs(cache_dir, eval_rev, settings.workers, expr, out_file)
    else:
        print(f"Cached eval of nixpkgs revision {eval_rev} exists")

