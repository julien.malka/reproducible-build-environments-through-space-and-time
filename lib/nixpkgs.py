import git
import os

def clone_nixpkgs(rev, path):
    """Perform a shallow clone of nixpkgs revision rev at path"""
    owd = os.getcwd()
    os.system(f"mkdir -p {path}/nixpkgs")
    os.chdir(f"{path}/nixpkgs")
    repo = git.Repo.init()
    try:
        origin = repo.create_remote("origin", "https://github.com/NixOS/nixpkgs.git")
    except:
        # remote already exists
        origin = repo.remotes.origin
        origin.set_url("https://github.com/NixOS/nixpkgs.git")
    print(f"Cloning revision {rev} into {path}/nixpkgs...")
    origin.fetch(refspec=rev, depth=1)
    repo.git.reset(rev, hard=True)
    print("Clone completed")
    os.chdir(owd)


